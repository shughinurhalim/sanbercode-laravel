<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<?php
		require 'animal.php';
		$sheep = new Animal("shaun");
		echo implode("<br>", $sheep->get_info());

		echo "<br><br>";

		$kodok = new Frog("buduk");
		echo implode("<br>", $kodok->get_info());
		echo "<br>";
		echo "Jump : " . $kodok->jump(); // "hop hop"

		echo "<br><br>";

		$sungokong = new Ape("kera sakti");
		echo implode("<br>", $sungokong->get_info());
		echo "<br>";
		echo "Yell : " . $sungokong->yell() . "<br>"; // "Auooo"
	?>
</body>
</html>
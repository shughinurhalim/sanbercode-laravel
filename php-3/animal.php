<?php 

class Animal
{
	protected $legs = 4;
	protected $cold_blooded = 'no';
	protected $name;

	public function __construct($name)
	{
		$this->name = $name;
	}

	public function get_info()
	{
		return [
			"Name : $this->name",
			"Legs : $this->legs",
			"Cold Blooded : $this->cold_blooded"
		];
	}
}

class Frog extends Animal
{
	public function jump()
	{
		return 'Hop Hop';
	}
}

class Ape extends Animal
{
	public function __construct($name)
	{
		$this->name = $name;
		$this->legs = 2;
	}

	public function yell()
	{
		return 'Auooo';
	}
}

?>
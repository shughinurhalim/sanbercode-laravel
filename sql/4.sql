-- a
SELECT
	id,
    name,
    email
FROM 
	users;


-- b
-- 1
SELECT
	*
FROM
	items
WHERE
	price > 1000000;

--2
SELECT
	*
FROM
	items
WHERE
	name LIKE '%sang%';


-- c
SELECT
	i.name,
    i.description,
    i.price,
    i.stock,
    i.category_id,
    c.name
FROM
	items i
JOIN
	categories c ON
		c.id = i.category_id
ORDER BY
	i.id ASC;
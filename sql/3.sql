INSERT INTO users
    (name, email, password)
VALUES 
    ('John Doe', 'john@doe.com', 'john123'),
    ('Jane Doe', 'jane@doe.com', 'jane123');

INSERT INTO categories
    (name)
VALUES
    ('gadget'),
    ('cloth'),
    ('men'),
    ('women'),
    ('branded');

INSERT INTO items
    (name, description, price, stock, category_id)
VALUES
    ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
    ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
    ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);
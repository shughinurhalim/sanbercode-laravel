@extends('layout.master')

@section('title')
Insert Cast
@endsection

@section('content')
    <div class="row mt-2">
        <div class="col-12">
            <h3>Edit Cast</h3>
            <form action="/cast/{{$cast->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="row mt-2 form-group">
                    <div class="col-6">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Insert Name" maxlength="45" value="{{$cast->name}}">
                        @error('name')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="col-6">
                        <label for="age">Age</label>
                        <input type="number" class="form-control numeric" name="age" id="age" placeholder="Insert Age" value="{{$cast->age}}">
                        @error('age')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2 form-group">
                    <div class="col-12">
                        <label for="bio">Bio</label>
                        <textarea name="bio" id="bio" cols="30" rows="10" class="form-control" placeholder="Insert Bio">{{$cast->bio}}</textarea>
                        @error('bio')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary float-end">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
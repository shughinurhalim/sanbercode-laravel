@extends('layout.master')

@section('title')
Show Cast
@endsection

@section('content')
    <div class="row mt-2">
        <h3>Cast Info</h3>
    </div>
    <div class="row">
        <div class="col-2">Name <span class="float-end">:</span></div>
        <div class="col-10">{{$cast->name}}</div>
    </div>
    <div class="row">
        <div class="col-2">Age <span class="float-end">:</span></div>
        <div class="col-10">{{$cast->age}}</div>
    </div>
    <div class="row">
        <div class="col-2">Bio <span class="float-end">:</span></div>
        <div class="col-10">{{$cast->bio}}</div>
    </div>
@endsection
@extends('layout.master')

@section('title')
Cast List
@endsection

@section('content')
    <div class="row mt-2">
        <div class="col-12">
            <h3>Cast List</h3>
            <a href="/cast/create" class="btn btn-primary"><i class="bi bi-plus"></i><span> Add</span></a>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Age</th>
                    <th scope="col">Bio</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($casts as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->name}}</td>
                            <td>{{$value->age}}</td>
                            <td>{{$value->bio}}</td>
                            <td>
                                <a href="/cast/{{$value->id}}" class="btn btn-warning"><i class="bi bi-eye"></i></a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-secondary"><i class="bi bi-pencil"></i></a>
                                <form action="/cast/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger"><i class="bi bi-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">No data</td>
                        </tr>  
                    @endforelse              
                </tbody>
            </table>
        </div>
    </div>

@endsection
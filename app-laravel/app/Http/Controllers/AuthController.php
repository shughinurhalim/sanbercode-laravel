<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register', [
            'error' => ''
        ]);
    }

    public function welcome(Request $request)
    {
        $name = $request['firstName'] . ' ' . $request['lastName'];

        return view('welcome', [
            'name' => $name
        ]);
    }
}

<html>
    <head>
        <title>Registration</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>

        <form id="formRegister" action="{{ route('welcome') }}" method="post">
            @csrf
            <p>First Name :</p>
            <input type="text" name="firstName">
            
            <p>Last Name :</p>
            <input type="text" name="lastName">
            
            <p>Gender :</p>
            <input type="radio" name="gender" value="Male">Male <br>
            <input type="radio" name="gender" value="Female">Female <br>
            <input type="radio" name="gender" value="Other">Other <br>
            
            <p>Nationality :</p>
            <select name="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="non-indonesia">Non Indonesia</option>
                <option value="other">Other</option>
            </select>

            <p>Language Spoken :</p>
            <input type="checkbox" name="language">Bahasa Indonesia <br>
            <input type="checkbox" name="language">English <br>
            <input type="checkbox" name="language">Other <br>

            <p>Bio :</p>
            <textarea name="bio" cols="30" rows="10"></textarea>

            <br><br>
            <input type="submit" value="Sign Up">
        </form>
    </body> 
</html>
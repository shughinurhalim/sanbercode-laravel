<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5

        $first_sentence_length = strlen($first_sentence);
        $second_sentence_length = strlen($second_sentence);

        $first_sentence_words = sizeof(explode(' ', $first_sentence));
        $second_sentence_words = sizeof(explode(' ', $second_sentence));


        echo "$first_sentence (Panjang String: $first_sentence_length | Jumlah Kata : $first_sentence_words) <br>";
        echo "$second_sentence (Panjang String: $second_sentence_length | Jumlah Kata : $second_sentence_words)";
        
        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        $string2_words = explode(' ', $string2);
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: $string2_words[1]" ;
        echo "<br> Kata Ketiga: $string2_words[2]" ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        $string3 = explode(' ', $string3);
        $string3[4] = 'awesome';
        $string3 = implode(' ', $string3);
        echo "String: \"$string3\" "; 
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>